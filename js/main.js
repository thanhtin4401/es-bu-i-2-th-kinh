let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];



function renderGlass(dataGlasses) {
    var HTML = ""
    for (var i = 0; i < dataGlasses.length; i++) {
        var glass = dataGlasses[i];
        HTML += `<div class="col-4" id="${glass.id}"onclick="changeGlass('${glass.virtualImg}','${glass.price}','${glass.brand}','${glass.name}','${glass.description}')">
        <img src="${glass.src}" alt="" style="width:100%">
    </div>`
    }
    document.getElementById('vglassesList').innerHTML = HTML;

}
renderGlass(dataGlasses);

let changeGlass = (Glass, price, brand, name, description) => {
    console.log('description: ', description);
    console.log(' name: ', name);
    console.log('brand: ', brand);
    console.log('price: ', price);
    let chGlass = `<img src="${Glass}" alt="">`
    let glassInfor = `
        <h1 class="header">${name} - ${brand}</h1>
        <div class="price_container d-flex align-items-center my-2">
            <h3 class="price mr-2 bg-danger rounded">$${price}</h3>
            <h3 class="stocking">stocking</h3>
        </div>
        <p class="title">${description}</p>
        `
    document.getElementById("avatar").innerHTML = chGlass;
    document.getElementById("glassesInfo").innerHTML = glassInfor
}
window.changeGlass = changeGlass;

let removeGlasses = (isTrue) => {
    if (isTrue == false) {
        document.querySelector('#avatar img').classList.add("d-none");
    }
    else {
        document.querySelector('#avatar img').classList.remove("d-none");
    }
}

window.removeGlasses = removeGlasses;